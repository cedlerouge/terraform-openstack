# Terraform-openstack

This is terraform samples to use with openstack backend

## Pre-requisite

* having terrform installed on your system


## Content

### TP-02

This deploys an external accessible openstack instance : 
- an internal network 
- an internal subnet
- a router which link internal network with external network (external)
- a floating IP 
- a port 
- a secgroup to allow incomming ssh
- instance

at this step no variable is used, see more next step

### TP-03

This is TP-02 with variables. 

### TP-04
