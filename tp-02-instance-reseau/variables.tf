# ===
# Variable définitions
# ===

variable "external_network" {
  type    = string
  default = "external"
}

variable "external_router" {
  type    = string
  default = "ext"
}

variable "dns_ip" {
  type    = list(string)
  default = ["208.67.222.222", "208.67.220.220"]
}
