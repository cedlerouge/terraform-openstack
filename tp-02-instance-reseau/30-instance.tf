data "template_file" "user_data" {
  template = file("./scripts/add-ssh.yaml")
}

resource "openstack_compute_instance_v2" "std_srv" {
  name              = "std_ssrv-3"
  image_name        = "imta-ubuntu-basic"
  flavor_name       = "m1.small"
  key_pair          = openstack_compute_keypair_v2.user_key.name
  security_groups   = ["ssh-http","default"]
  user_data         = data.template_file.user_data.rendered
  network {
      name = openstack_networking_network_v2.internal_net.name
  }


}

