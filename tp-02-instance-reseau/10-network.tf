# ===
# Network
# ===

data "openstack_networking_network_v2" "ext_network" {
  name = var.external_network
}

data "openstack_networking_router_v2" "ext_router" {
  name = var.external_router
}

resource "openstack_networking_network_v2" "internal_net" {
  name = "internal_net"
  admin_state_up      = true
}

resource "openstack_networking_subnet_v2" "internal_sub" {
  name            = "internal_sub"
  network_id      = openstack_networking_network_v2.internal_net.id
  cidr            = "192.168.199.0/24"
  dns_nameservers = var.dns_ip
}

# Router interface configuration
resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = data.openstack_networking_router_v2.ext_router.id
  subnet_id = openstack_networking_subnet_v2.internal_sub.id
}

# Create floating ip
resource "openstack_networking_floatingip_v2" "fip_ssh" {
  pool = "external"
}

# Attach floating ip to instance
resource "openstack_compute_floatingip_associate_v2" "http" {
  floating_ip = openstack_networking_floatingip_v2.fip_ssh.address
  instance_id = openstack_compute_instance_v2.std_srv.id
}
