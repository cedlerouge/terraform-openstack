# ===
# Security Groups
# ===


resource "openstack_compute_secgroup_v2" "ssh-http" {
  name        = "ssh-http"
  description = "Open input ssh and http ports"
  
  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}
